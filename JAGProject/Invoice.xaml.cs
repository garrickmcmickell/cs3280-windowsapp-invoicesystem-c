﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JAGProject
{
    /// <summary>
    /// Interaction logic for Invoice.xaml
    /// </summary>
    public partial class Invoice : Window
    {

        #region Methods

        /// <summary>
        /// Initialized the window and links to the InvoiceWindowOptions so the window can
        /// be modified.
        /// </summary>
        public Invoice()
        {
            try
            {
                InitializeComponent();
                InvoiceWindowOptions.invoiceWindow = this;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the save button is clicked. Saves the current invoice.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveNewInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                InvoiceWindowOptions.saveInvoice();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the edit button is clicked. Enables editing of the
        /// current invoice.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEditNewInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                InvoiceWindowOptions.enableEditing();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the delete button is clicked. Deletes the current
        /// invoice and resets the window. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteNewInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                InvoiceWindowOptions.deleteInvoice();
                InvoiceWindowOptions.resetWindow();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the add item button is clicked. Adds the item and
        /// checks if the current invoice can be saved.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                InvoiceWindowOptions.addItem();

                InvoiceWindowOptions.checkCanSave();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the window is closed. Hides the window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                this.Hide();
                e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the date is changed. Changes the date of the current
        /// invoice and checks to see if it can be saved.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpNewInvoiceDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                InvoiceWindowOptions.changeDate();

                InvoiceWindowOptions.checkCanSave();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Method to handle what happens when the delete button is clicked. Deletes the selected
        /// item and check to see if the current invoice can be saved. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                InvoiceWindowOptions.deleteItem();

                InvoiceWindowOptions.checkCanSave();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Method to handle what happens when the selection is changed in the item combobox.
        /// Checks to see if the item can be added to the current invoice.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbbItemPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                InvoiceWindowOptions.checkCanAddItem();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Method that handles what happens when the selection is changed in the groupbox.
        /// Checks to see if the item can be deleted from the current invoice.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dbNewInvoice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                InvoiceWindowOptions.checkCanDeleteItem();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        #endregion

    }
}
