﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JAGProject
{
    /// <summary>
    /// Interaction logic for SearchInvoice.xaml
    /// </summary>
    public partial class SearchInvoice : Window
    {
        #region Class Level Variables

        /// <summary>
        /// Used to store a copy of the main window so that the selected invoice can be
        /// passed back to it. 
        /// </summary>
        public MainWindow mainWindow { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the window and links it to SeachWindowOptions to be edited.
        /// </summary>
        public SearchInvoice()
        {
            try
            {
                InitializeComponent();

                SearchWindowOptions.searchWindow = this;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the select button is clicked. Sends the selected invoice
        /// to the main window. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                mainWindow.currentInvoice = (InvoiceObject)dgSearchGrid.SelectedItem;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the search button is clicked. Searched for an invoice
        /// that matched the entered values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SearchWindowOptions.searchInvoice();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the window is closed. Hides the window. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                this.Hide();
                e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the reset button is clicked. Resets the window to its'
        /// original state. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnResetSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SearchWindowOptions.resetSearch();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the selection is changed in the total combobox. Checks
        /// to see a search can be performed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbTotalCharged_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SearchWindowOptions.canSearch();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the selected date is changed. Checks to see if a search
        /// can be performed. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dpDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SearchWindowOptions.canSearch();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the selection is changed in the invoice ID combobox.
        /// Check to see if a search can be performed. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbInvoiceID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SearchWindowOptions.canSearch();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the selection is changed in the groupbox. Checks to see
        /// if the invoice can be selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgSearchGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SearchWindowOptions.canSelect();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        #endregion
    }
}
