﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JAGProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Invoice invoiceWindow = new Invoice();
        SearchInvoice searchWindow = new SearchInvoice();

        /// <summary>
        /// InvoiceObject to store the invoice selected in the SearchInvoice window.
        /// </summary>
        public InvoiceObject currentInvoice { get; set; }

        public MainWindow()
        {
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

            InitializeComponent();

            // Passes a copy of this window so the selected invoice can be passed
            // back from the SearchInvoice window.
            searchWindow.mainWindow = this;
        }

        private void btnNewInvoice_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            // This method call resets everything in the Invoice window.
            InvoiceWindowOptions.resetWindow();
            invoiceWindow.ShowDialog();
            this.Show();
        }

        private void btnSearchInvoice_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            // This method call resets everything in the SearchInvoice window.
            SearchWindowOptions.resetWindow();
            searchWindow.ShowDialog();
            this.Show();
        }

        private void btnUpdateItemTable_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnEditInvoice_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnDeleteInvoice_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
