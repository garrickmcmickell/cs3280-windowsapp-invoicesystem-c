﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAGProject
{
    public class ItemObject
    {
        public string sItemName { get; set; }
        public string sItemCost { get; set; }
        public string sItemDesc { get; set; }

        public override string ToString()
        {
            return sItemName;
        }
    }
}
