﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JAGProject
{
    public static class InvoiceWindowOptions
    {
        #region Class Level Variables

            #region Public Variables

        /// <summary>
        /// Used to store a copy of the Invoice window to be used for modification of the
        /// window's elements.
        /// </summary>
        public static Invoice invoiceWindow { get; set; }

        #endregion

            #region Private Variables

        /// <summary>
        /// Used to access and use methods of the clsDataAccess class.
        /// </summary>
        private static clsDataAccess dataAccess = new clsDataAccess();

        /// <summary>
        /// Used to store the retrieved ItemObjects from the database.
        /// </summary>
        private static List<ItemObject> itemList = new List<ItemObject>();

        /// <summary>
        /// Used to store an Invoice so that it can be saved to the database.
        /// </summary>
        private static InvoiceObject currentInvoice;

        /// <summary>
        /// Used to store the next InvoiceID to be used to create an invoice.
        /// </summary>
        private static int iInvoiceId;      
        
        /// <summary>
        /// Used to determine if an invoice has been saved. 
        /// </summary>        
        private static bool bSaved;

        /// <summary>
        /// Used to determine if a created invoice is currently being edited. 
        /// </summary>
        private static bool bEditing;

        #endregion

        #endregion

        #region Methods

            #region Public Methods

        /// <summary>
        /// Depending on if an invoice has been saved, this will either save an invoice or
        /// delete the previously saved invoice and saves the edited version, then determines
        /// what buttons should be active.
        /// </summary>
        public static void saveInvoice()
        {
            try
            {
                if (bEditing == false)
                {
                    saveNewInvoice();
                }
                else
                {
                    deleteInvoice();
                    saveNewInvoice();
                    bEditing = false;
                }

                checkCanSave();
                checkCanEdit();
                checkCanDelete();
                checkCanAddItem();
                checkCanDeleteItem();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Deletes the current invoice from the database.
        /// </summary>
        public static void deleteInvoice()
        {
            try
            {
                string sSQL = "DELETE FROM Invoice WHERE InvoiceID = " + iInvoiceId;

                dataAccess.ExecuteNonQuery(sSQL);
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Adds an item to the current invoice and updates the total. 
        /// </summary>
        public static void addItem()
        {
            try
            {
                ItemObject item = (ItemObject)invoiceWindow.cbbItemPicker.SelectedItem;

                currentInvoice.itemList.Add(item);

                updateCurrentTotal();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Deletes the selected item from the current invoice and updates the total. 
        /// </summary>
        public static void deleteItem()
        {
            try
            {
                if (invoiceWindow.dbNewInvoice.SelectedItem != null && bSaved == false)
                {
                    currentInvoice.itemList.Remove((ItemObject)invoiceWindow.dbNewInvoice.SelectedItem);
                }

                updateCurrentTotal();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Determines if the selected date is valid, and if so, changes the current invoices
        /// date to match the selection.
        /// </summary>
        public static void changeDate()
        {
            try
            {
                DateTime? checkDate = invoiceWindow.dtpNewInvoiceDate.SelectedDate;

                if (checkDate != null)
                {
                    currentInvoice.sDate = checkDate.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Determines if the current invoice can be saved.
        /// </summary>
        public static void checkCanSave()
        {
            try
            {
                if (currentInvoice.sDate == null || currentInvoice.itemList.Count == 0 || bSaved == true)
                    invoiceWindow.btnSaveNewInvoice.IsEnabled = false;
                else
                    invoiceWindow.btnSaveNewInvoice.IsEnabled = true;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Determines if the current invoice can be edited.
        /// </summary>
        public static void checkCanEdit()
        {
            try
            {
                if (bSaved == true && bEditing == false)
                {
                    invoiceWindow.btnEditNewInvoice.IsEnabled = true;
                }
                else
                {
                    invoiceWindow.btnEditNewInvoice.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Determines if the current invoice can be deleted.
        /// </summary>
        public static void checkCanDelete()
        {
            try
            {
                if (bSaved == true && bEditing == false)
                {
                    invoiceWindow.btnDeleteNewInvoice.IsEnabled = true;
                }
                else
                {
                    invoiceWindow.btnDeleteNewInvoice.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Determines if an item can be added to the current invoice.
        /// </summary>
        public static void checkCanAddItem()
        {
            try
            {
                if (invoiceWindow.cbbItemPicker.SelectedItem == null || bSaved == true)
                {
                    invoiceWindow.btnAddItem.IsEnabled = false;
                }
                else
                {
                    invoiceWindow.btnAddItem.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Determines if items can be deleted from the current invoice.
        /// </summary>
        public static void checkCanDeleteItem()
        {
            try
            {
                if (invoiceWindow.dbNewInvoice.SelectedItem == null || bSaved == true)
                {
                    invoiceWindow.btnDeleteItem.IsEnabled = false;
                }
                else
                {
                    invoiceWindow.btnDeleteItem.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Enables editing of the current invoice and checks to see what options can be 
        /// set on the page.
        /// </summary>
        public static void enableEditing()
        {
            try
            {
                bEditing = true;
                bSaved = false;

                checkCanSave();
                checkCanEdit();
                checkCanDelete();
                checkCanAddItem();
                checkCanDeleteItem();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Resets the Invoice window to its' original state.
        /// </summary>
        public static void resetWindow()
        {
            try
            {
                currentInvoice = new InvoiceObject();

                invoiceWindow.dbNewInvoice.ItemsSource = currentInvoice.itemList;
                invoiceWindow.lblNewCurrentTotal.Content = "Total Cost: $0";
                invoiceWindow.dtpNewInvoiceDate.SelectedDate = null;
                invoiceWindow.cbbItemPicker.SelectedItem = null;

                checkCanSave();
                checkCanEdit();
                checkCanDelete();
                checkCanAddItem();
                checkCanDeleteItem();

                bSaved = false;
                bEditing = false;

                string sSQL = "SELECT MAX(Invoice.InvoiceID) FROM Invoice";

                iInvoiceId = int.Parse(dataAccess.ExecuteScalarSQL(sSQL)) + 1;

                invoiceWindow.lblInvoiceID.Content = "Invoice ID: " + iInvoiceId;

                getItems();

                foreach (ItemObject item in itemList)
                {
                    invoiceWindow.cbbItemPicker.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

            #endregion

            #region Private Methods

        /// <summary>
        /// Creates a string with the current invoice's information and sends it to clsDataAccess
        /// to be added to the database.
        /// </summary>
        private static void saveNewInvoice()
        {
            try
            {
                string sSQL = "INSERT INTO INVOICE(InvoiceID, TotalCost, InvoiceDate) VALUES( " + iInvoiceId + ", '" + double.Parse(currentInvoice.sTotal) + "', '" + DateTime.Parse(currentInvoice.sDate) + "')";

                dataAccess.ExecuteNonQuery(sSQL);

                int i = 1;
                foreach (ItemObject item in currentInvoice.itemList)
                {
                    sSQL = "INSERT INTO LINEITEMS(InvoiceID, LineItem, ItemName, ItemCost) VALUES(" + iInvoiceId + ", " + i + ", '" + item.sItemName + "', " + double.Parse(item.sItemCost) + ")";

                    dataAccess.ExecuteNonQuery(sSQL);

                    i++;
                }

                bSaved = true;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Gets a list of all items available and adds them to the items combobox.
        /// </summary>
        private static void getItems()
        {
            try
            {
                foreach (ItemObject i in itemList)
                {
                    invoiceWindow.cbbItemPicker.Items.Remove(i);
                }

                itemList = new List<ItemObject>();

                string sSQL = "SELECT ItemDesc.ItemName, ItemDesc.ItemCost, ItemDesc.ItemDesc " +
                              "FROM ItemDesc ";
                int iRet = 0;

                DataSet itemTable = new DataSet();

                ItemObject item;

                itemTable = dataAccess.ExecuteSQLStatement(sSQL, ref iRet);

                for (int i = 0; i < iRet; i++)
                {
                    item = new ItemObject();

                    item.sItemName = itemTable.Tables[0].Rows[i][0].ToString();
                    item.sItemCost = itemTable.Tables[0].Rows[i][1].ToString();
                    item.sItemDesc = itemTable.Tables[0].Rows[i][2].ToString();

                    itemList.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Updates the total of the current invoice.
        /// </summary>
        private static void updateCurrentTotal()
        {
            try
            {
                double total = 0;

                foreach (ItemObject item in currentInvoice.itemList)
                {
                    total += double.Parse(item.sItemCost);
                }

                currentInvoice.sTotal = total.ToString();

                invoiceWindow.lblNewCurrentTotal.Content = "Total Cost: $" + total;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

            #endregion

        #endregion

    }
}
