﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JAGProject
{
    public static class SearchWindowOptions
    {
        #region Class Level Variables

            #region Public Variables

        /// <summary>
        /// Used to store a copy of the SearchInvoice window.
        /// </summary>
        public static SearchInvoice searchWindow;

            #endregion

            #region Private Variables

        /// <summary>
        /// Used to access and use methods of the clsDataAccess class.
        /// </summary>
        private static clsDataAccess dataAccess = new clsDataAccess();

        /// <summary>
        /// Used to store the retrieved invoice IDs from the database.
        /// </summary>
        private static List<string> invoiceIDList = new List<string>();

        /// <summary>
        /// Used to store the retrieved invoice totals from the database.
        /// </summary>
        private static List<string> totalsList = new List<string>();

        #endregion

        #endregion

        #region Methods

            #region Public Methods

        /// <summary>
        /// Calls required methods to return the SearchInvoice window to its' original
        /// state.
        /// </summary>
        public static void resetWindow()
        {
            try
            {
                resetSearch();
                canSearch();
                canSelect();
                getInvoiceIDs();
                getTotals();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Sets the selections for each field to original state and removes items from
        /// the DataGrid.
        /// </summary>
        public static void resetSearch()
        {
            try
            {
                searchWindow.cbInvoiceID.SelectedIndex = -1;
                searchWindow.cbTotalCharged.SelectedIndex = -1;
                searchWindow.dpDate.SelectedDate = null;
                searchWindow.dgSearchGrid.ItemsSource = null;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Checks to see if all search option fields are empty. If they are, it disables
        /// the search and reset buttons. If not, it enables them.
        /// </summary>
        public static void canSearch()
        {
            try
            {
                if (searchWindow.cbInvoiceID.SelectedItem == null && searchWindow.dpDate.SelectedDate == null && searchWindow.cbTotalCharged.SelectedItem == null)
                {
                    searchWindow.btnSearch.IsEnabled = false;
                    searchWindow.btnResetSearch.IsEnabled = false;
                }
                else
                {
                    searchWindow.btnSearch.IsEnabled = true;
                    searchWindow.btnResetSearch.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Checks to see if there is a selected item in the DataGrid. If there is, it 
        /// enables the select button. If not, it disables it.
        /// </summary>
        public static void canSelect()
        {
            try
            {
                if (searchWindow.dgSearchGrid.SelectedItem == null)
                {
                    searchWindow.btnSelectInvoice.IsEnabled = false;
                }
                else
                {
                    searchWindow.btnSelectInvoice.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Using the search parameters, this method creates a string and sends it to
        /// clsDataAccess. Using the returned results, it creates an ObservableCollection
        /// of InvoiceObjects and links them to the DataGrid so they can be displayed.
        /// </summary>
        public static void searchInvoice()
        {
            try
            {
                List<string> lstSearchParameters = new List<string>();
                string sSearchParameters = "";
                int iInvoiceID;
                double dTotalCharged;

                if (int.TryParse(searchWindow.cbInvoiceID.Text, out iInvoiceID))
                {
                    lstSearchParameters.Add("InvoiceID = " + iInvoiceID);
                }

                if (searchWindow.dpDate.SelectedDate != null)
                {
                    lstSearchParameters.Add("InvoiceDate = #" + searchWindow.dpDate.SelectedDate.Value.ToString() + "#");
                }

                if (double.TryParse(searchWindow.cbTotalCharged.Text, out dTotalCharged))
                {
                    lstSearchParameters.Add("TotalCost = " + dTotalCharged);
                }

                if (lstSearchParameters.Count > 1)
                {
                    foreach (string searchParameter in lstSearchParameters)
                    {
                        if (lstSearchParameters.IndexOf(searchParameter) < lstSearchParameters.Count - 1)
                        {
                            sSearchParameters += searchParameter + "AND ";
                        }
                        else
                        {
                            sSearchParameters += searchParameter;
                        }
                    }
                }
                else if (lstSearchParameters.Count == 1)
                {
                    sSearchParameters += lstSearchParameters[0];
                }

                string sSQL = "SELECT InvoiceID, InvoiceDate, TotalCost " +
                              "FROM Invoice " +
                              "WHERE " + sSearchParameters;
                int iRet = 0;

                DataSet invoiceTable = new DataSet();

                ObservableCollection<InvoiceObject> invoices = new ObservableCollection<InvoiceObject>();
                InvoiceObject invoice;

                searchWindow.dgSearchGrid.ItemsSource = invoices;

                invoiceTable = dataAccess.ExecuteSQLStatement(sSQL, ref iRet);

                for (int i = 0; i < iRet; i++)
                {
                    invoice = new InvoiceObject();

                    invoice.sInvoiceID = invoiceTable.Tables[0].Rows[i][0].ToString();
                    invoice.sDate = invoiceTable.Tables[0].Rows[i][1].ToString();
                    invoice.sTotal = invoiceTable.Tables[0].Rows[i][2].ToString();

                    invoices.Add(invoice);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        #endregion

            #region Private Methods

        /// <summary>
        /// Retrives a list of all invoice IDs and saves them to a ComboBox to be selected
        /// as search parameters.
        /// </summary>
        private static void getInvoiceIDs()
        {
            try
            {
                foreach (string invoiceID in invoiceIDList)
                {
                    searchWindow.cbInvoiceID.Items.Remove(invoiceID);
                }

                invoiceIDList = new List<string>();

                DataSet invoiceIDs = new DataSet();

                string sSQL = "SELECT InvoiceID FROM Invoice";
                int iRet = 0;

                invoiceIDs = dataAccess.ExecuteSQLStatement(sSQL, ref iRet);

                for (int i = 0; i < iRet; i++)
                {
                    invoiceIDList.Add(invoiceIDs.Tables[0].Rows[i][0].ToString());
                }

                foreach (string invoiceID in invoiceIDList)
                {
                    searchWindow.cbInvoiceID.Items.Add(invoiceID);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Retrives a list of all invoice totals and saves them to a ComboBox to be selected
        /// as search parameters.
        /// </summary>
        private static void getTotals()
        {
            try
            {
                foreach (string total in totalsList)
                {
                    searchWindow.cbTotalCharged.Items.Remove(total);
                }

                totalsList = new List<string>();

                DataSet totals = new DataSet();

                string sSQL = "SELECT DISTINCT TotalCost FROM Invoice";
                int iRet = 0;

                totals = dataAccess.ExecuteSQLStatement(sSQL, ref iRet);

                for (int i = 0; i < iRet; i++)
                {
                    totalsList.Add(totals.Tables[0].Rows[i][0].ToString());
                }

                foreach (string total in totalsList)
                {
                    searchWindow.cbTotalCharged.Items.Add(total);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

            #endregion

        #endregion
    }
}
