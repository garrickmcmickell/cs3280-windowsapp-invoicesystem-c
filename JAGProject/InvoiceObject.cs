﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAGProject
{
    public class InvoiceObject
    {
        /// <summary>
        /// Used to store the items on the invoice.
        /// </summary>
        public ObservableCollection<ItemObject> itemList = new ObservableCollection<ItemObject>();

        /// <summary>
        /// Used to store the Invoice ID
        /// </summary>
        public string sInvoiceID { get; set; }

        /// <summary>
        /// Used to store the Invoice Date.
        /// </summary>
        public string sDate { get; set; }

        /// <summary>
        /// Used to store the invoice total.
        /// </summary>
        public string sTotal { get; set; }
    }
}
